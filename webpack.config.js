const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
////////////////

module.exports = {
    context: path.resolve(__dirname, 'src'),
    mode: 'development',
    entry: {
        main: './js/index.js',
        analytics: './js/analytic.js',
    },
    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve(__dirname, 'dist'),
    },
    resolve: {
        extensions: ['.js'], // allow us wite import .. from 'Post' (not 'Post.js')
        alias: {
            '@': path.resolve(__dirname, 'src'), // absolute path
        },
        
    },
    optimization: {
        // optimize file loading (if you import jQuery 2 and more time)
        splitChunks: {
            chunks: 'all',
        },
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html',
        }),
        new CleanWebpackPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(png | svg | jpg | gif)$/,
                use: ['file-loader'],
            },
            {
                test: /\.ttf$/,
                use: ['file-loader']
            }
        ],
    }

}