function createAnalytic() {
    let counter = 0;
    const listener = () => counter++;
    let isDestroyed = false;
    document.addEventListener('click', listener);

    return {
        destroy: () => document.removeEventListener('click', listener),
        getClick: () => {
            if (isDestroyed) {
                return 'loh';
            }
            return counter;
        }
    }
}

window.anal = createAnalytic();